package com.designpattern.creational.builder;

public abstract class CD implements Packing{
	public abstract String pack();
}
