package com.arrays;

import java.util.Arrays;

public class CompareTwoArrays {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int arr1[] = {1, 2, 3};
        int arr2[] = {1, 2, 3};
        if (arr1 == arr2) // Same as arr1.equals(arr2)
            System.out.println("== : Same");
        else
            System.out.println("== : Not same");
        
        
        if (arr1.equals(arr2))
            System.out.println(".equals : Same");
        else
            System.out.println(".equals : Not same");
        
        
        if (Arrays.equals(arr1, arr2))
            System.out.println("Arrays.equals : Same");
        else
            System.out.println("Arrays.equals : Not same");

        
        int inarr1[] = {1, 2, 3};
        int inarr2[] = {1, 2, 3};   
        Object[] array1 = {inarr1};  // arr1 contains only one element
        Object[] array2 = {inarr2};  // arr2 also contains only one element
        if (Arrays.equals(array1, array2))
            System.out.println("Array in Array - Arrays.equals : Same");
        else
            System.out.println("Array in Array - Arrays.equals : Not same");
        
        
        if (Arrays.deepEquals(array1, array2))
            System.out.println("Array in Array - Arrays.deepEquals : Same");
        else
            System.out.println("Array in Array - Arrays.deepEquals : Not same");
        
        System.out.println("-------------------------------------------------------------------------------");
        
        MyBean b1 = new MyBean(10, 20);
        MyBean b2 = new MyBean(10, 20);
        MyBean b3 = new MyBean(10, 60);
        
        MyBean[] myBeansArr1 = {b1,b2};
        MyBean[] myBeansArr2 = {b1,b2};
        MyBean[] myBeansArr3 = {b2,b3};
        
        if (Arrays.deepEquals(myBeansArr1, myBeansArr2))
            System.out.println("myBeansArr1 and myBeansArr2 are : Same");
        else 
        	System.out.println("myBeansArr1 and myBeansArr2 are : Not Same");
        
        if (Arrays.deepEquals(myBeansArr2, myBeansArr3))
            System.out.println("myBeansArr2 and myBeansArr3 are  : same");
        else 
        	System.out.println("myBeansArr2 and myBeansArr3 are : Not Same");
        
        if (Arrays.deepEquals(myBeansArr1, myBeansArr3))
            System.out.println("myBeansArr1 and myBeansArr3 are  : same");
        else 
        	System.out.println("myBeansArr1 and myBeansArr3 are : Not Same");
        
        System.out.println("-------------------------------------------------------------------------------");
        
        int inarr101[] = {1, 2, 3};
        int inarr201[] = {1, 2, 3}; 
        Object[] arr101 = {inarr101};  // arr1 contains only one element
        Object[] arr201 = {inarr201};  // arr2 also contains only one element
        Object[] outarr1 = {arr101}; // outarr1 contains only one element
        Object[] outarr2 = {arr201}; // outarr2 also contains only one element        
        if (Arrays.deepEquals(outarr1, outarr2))
            System.out.println("Same");
        else
            System.out.println("Not same");
        
	}

}
