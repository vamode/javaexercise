package com.collection.map;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;

public class DictionaryDemo {
	
	/*
	
	util.Dictionary is an abstract class, representing a key-value relation and works similiar to a map. 
	Given a key you can store values and when needed can retrieve the value back using its key. 
	Thus, it is a list of key-value pair.
	
	
	The Dictionary class is the abstract parent of any class, such as Hashtable, which maps keys to values. 
	Every key and every value is an object. In any one Dictionary object, every key is associated with at most one value. 
	Given a Dictionary and a key, the associated element can be looked up. Any non-null object can be used as a key and as a value.
	
	As a rule, the equals method should be used by implementations of this class to decide if two keys are the same.
	
	NOTE: This class is obsolete. New implementations should implement the Map interface, rather than extending this class.
		
	*/

	public static void main(String[] args) {
		
		// Initializing a Dictionary
        Dictionary geek = new Hashtable();
 
        // put() method
        geek.put("123", "Code");
        geek.put("456", "Program");
 
        // elements() method :
        for (Enumeration i = geek.elements(); i.hasMoreElements();)
        {
            System.out.println("Value in Dictionary : " + i.nextElement());
        }
 
        // get() method :
        System.out.println("\nValue at key = 6 : " + geek.get("6"));
        System.out.println("Value at key = 456 : " + geek.get("123"));
 
        // isEmpty() method :
        System.out.println("\nThere is no key-value pair : " + geek.isEmpty() + "\n");
 
        // keys() method :
        for (Enumeration k = geek.keys(); k.hasMoreElements();)
        {
            System.out.println("Keys in Dictionary : " + k.nextElement());
        }
 
        // remove() method :
        System.out.println("\nRemove : " + geek.remove("123"));
        System.out.println("Check the value of removed key : " + geek.get("123"));
 
        System.out.println("\nSize of Dictionary : " + geek.size());

	}

}
