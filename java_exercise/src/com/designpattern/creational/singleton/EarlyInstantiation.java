package com.designpattern.creational.singleton;

public class EarlyInstantiation {
	
	private static EarlyInstantiation instantiation = new EarlyInstantiation(); // Early, instance will be created at load time

	private EarlyInstantiation() {}
	
	public static EarlyInstantiation getInstance(){
		return instantiation;
	}
	
	/*
	 
	If singleton class is Serializable, you can serialize the singleton instance. 
	Once it is serialized, you can deserialize it but it will not return the singleton object.

	To resolve this issue, you need to override the readResolve() method that enforces the singleton. 
	It is called just after the object is deserialized. It returns the singleton object.

	*/
	
	protected Object readResolve(){
		return getInstance();
	}
	
	public void doSomthing(){
		// write code.
	}

}
