package com.collection.map;

import java.util.LinkedHashMap;

public class LinkedHashMapDemo {
	
	/*
	
	HashMap in Java provides quick insert, search and delete operations. However it does not maintain any 
	order on elements inserted into it. If we want to keep track of order of insertion, we can use LinkedHashMap.
	
	LinkedHashMap is like HashMap with additional feature that we can access elements in their insertion order.
	
	LinkedHashMap<Integer, String> lhm = new LinkedHashMap<Integer, String>();
	
	- A LinkedHashMap contains values based on the key. It implements the Map interface and extends HashMap class.
	- It contains only unique elements 
	- It may have one null key and multiple null values 
	- It is same as HashMap with additional feature that it maintains insertion order. 
		For example, when we ran the code with HashMap, we got different oder of elements
		
	
	*/

	public static void main(String[] args) {
		
		LinkedHashMap<String, String> lhm =
                new LinkedHashMap<String, String>();
		 lhm.put("one", "practice.geeksforgeeks.org");
		 lhm.put("two", "code.geeksforgeeks.org");
		 lhm.put("four", "quiz.geeksforgeeks.org");
		
		 // It prints the elements in same order as they were inserted    
		 System.out.println(lhm);
		
		 System.out.println("Getting value for key 'one': " + lhm.get("one"));
		 System.out.println("Size of the map: " + lhm.size());
		 System.out.println("Is map empty? " + lhm.isEmpty());
		 System.out.println("Contains key 'two'? "+ lhm.containsKey("two"));
		 System.out.println("Contains value 'practice.geeksforgeeks.org'? "
		                    + lhm.containsValue("practice.geeksforgeeks.org"));
		 System.out.println("delete element 'one': " + lhm.remove("one"));
		 System.out.println(lhm);

	}

}
