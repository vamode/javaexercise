package com.collection.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListDemo {
	
	/*
		Java.util.List is a child interface of Collection.
		
		List is an ordered collection of objects in which duplicate values can be stored. 
		Since List preserves the insertion order it allows positional access and insertion of elements. 
		List Interface is implemented by ArrayList, LinkedList, Vector and Stack classes.
		
		List Interface is the subinterface of Collection.It contains methods to insert and delete elements in index basis.
		It is a factory of ListIterator interface.
		
		ListIterator Interface is used to traverse the element in backward and forward direction.
		
		Generic List Object:
			- After the introduction of Generics in Java 1.5, it is possible to restrict the type of object 
			  that can be stored in the List. We can declare type safe List in following way:

			  // Student is type of object to be stored in List.
			  List<Student> list = new ArrayList<Student> ();
		
		ListIterator Interface declaration : 
			public interface ListIterator<E> extends Iterator<E>  
			  
	*/

	public static void main(String[] args) {
		
		/*
		
		Operations on List:
	   		- List Interface extends Collection, hence it supports all the operations of Collection Interface 
	   		  and along with following operations:
	   		  
	   		  1. Positional Access:
	   		  2. Search:
	   		  3. Iteration:
	   		  4. Range-view:
		
		*/
		positionalAccess();
		search();
		iteration();
		rangeView();
		
	}

	private static void positionalAccess() {
		
		/*
		
		1. Positional Access:
					List allows add, remove, get and set operations based on numerical positions of elements in List.
		
		*/
		
		System.out.println("-------------- Positional Access -----------------");
		// Let us create a list
        List l1 = new ArrayList();
        l1.add(0, 1);  // adds 1 at 0 index
        l1.add(1, 2);  // adds 2 at 1 index
        System.out.println(l1);  // [1, 2]
 
        // Let us create another list
        List l2 = new ArrayList();
        l2.add(1);
        l2.add(2);
        l2.add(3);
 
        // will add list l2 from 1 index
        l1.addAll(1, l2);
        System.out.println(l1);
 
        l1.remove(1);     // remove element from index 1
        System.out.println(l1); // [1, 2, 3, 2]
 
        // prints element at index 3
        System.out.println(l1.get(3));
 
        l1.set(0, 5);   // replace 0th element with 5
        System.out.println(l1);  // [5, 2, 3, 2]
		
		
	}
	
	private static void search() {
		
		/*
		
		 2. Search:
	   		  		List provides methods to search element and returns its numeric position.
		
		*/
		
		System.out.println("-------------- Search -----------------");
		
		// type safe array list, stores only string
        List<String> l = new ArrayList<String>(5);
        l.add("Geeks");
        l.add("for");
        l.add("Geeks");
 
        // Using indexOf() and lastIndexOf()
        System.out.println("first index of Geeks:" +
                                  l.indexOf("Geeks"));
        System.out.println("last index of Geeks:" +
                               l.lastIndexOf("Geeks"));
        System.out.println("Index of element not present : " +
                                l.indexOf("Hello"));
	}

	private static void iteration() {
		
		/*
		
		3. Iteration:	
	   		  		ListIterator(extends Iterator) is used to iterate over List element. List iterator is bidirectional iterator.
		
		*/
		
		System.out.println("-------------- Iteration -----------------");
		
		ArrayList al = new ArrayList();
		
		for (int i = 0; i < 10; i++)
            al.add(i);
		 
        // at beginning itr(cursor) will point to
        // index just before the first element in al
        Iterator itr = al.iterator();
 
        // checking the next element availabilty
        while (itr.hasNext())
        {
            //  moving cursor to next element
            int i = (Integer)itr.next();
 
            // getting even elements one by one
            System.out.println(i + " ");
            
        }
		
	}
	
	private static void rangeView() {
		
		/*
		
		4. Range-view:
			  		List Interface provides method to get List view of the portion of given List between two indices
		
		*/
		
		System.out.println("-------------- Range-View -----------------");
		
		// Type safe array list, stores only string
        List<String> l = new ArrayList<String>(5);
 
        l.add("GeeksforGeeks");
        l.add("Practice");
        l.add("GeeksQuiz");
        l.add("IDE");
        l.add("Courses");
 
        List<String> range = new ArrayList<String>();
 
        // return List between 2nd(including)
        // and 4th element(excluding)
        range = l.subList(2, 4);
 
        System.out.println(range);  //
		
	}

}
