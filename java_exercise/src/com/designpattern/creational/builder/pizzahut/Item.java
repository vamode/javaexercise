package com.designpattern.creational.builder.pizzahut;

public interface Item {
	public String name();
	public String size();
	public float price();
}
