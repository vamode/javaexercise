package com.designpattern.creational.builder.pizzahut;

public class LargeCheezePizza extends VegPizza{

	@Override
	public String name() {
		return "Cheeze Pizza";
	}

	@Override
	public String size() {
		return "Large Size";
	}

	@Override
	public float price() {
		return 260.0f;
	}

}
