package com.designpattern.creational.singleton;

public class LazyInstantiation {
	private static LazyInstantiation instantiation;
	
	private LazyInstantiation(){}
	
	public static LazyInstantiation getLazyInstantiation(){
		if(null == instantiation){
			synchronized (LazyInstantiation.class) {
				if(null == instantiation){
					instantiation = new LazyInstantiation(); //instance will be created at request time  
				}
			}
		}
		
		return instantiation;
	}
	
	public void doSomthing(){
		// write code
	}

}
