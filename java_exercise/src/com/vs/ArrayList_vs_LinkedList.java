package com.vs;

import java.util.ArrayList;
import java.util.LinkedList;

public class ArrayList_vs_LinkedList {
	
	/*
	
	Comparision between ArrayList and LinkedList:-

		- Insertions are easy and fast in LinkedList as compared to ArrayList because there is no
		  risk of resizing array and copying content to new array if array gets full which makes
		  adding into ArrayList of O(n) in worst case, while adding is O(1) operation in LinkedList
		  in Java. ArrayList also needs to be update its index if you insert something anywhere except
		  at the end of array.
		
		- Removal also better in LinkedList than ArrayList due to same reasons as insertion.
		
		- LinkedList has more memory overhead than ArrayList because in ArrayList each index only
		  holds actual object (data) but in case of LinkedList each node holds both data and address
		  of next and previous node.
		
		- Both LinkedList and ArrayList require O(n) time to find if an element is present or not. However we can do Binary Search on ArrayList if it is sorted and therefore can search in O(Log n) time.
	
	*/
	
	/*
	
	Difference between ArrayList and LinkedList : 
		ArrayList and LinkedList both implements List interface and maintains insertion order. Both are non synchronized classes.
		
		ArrayList : 
			1) ArrayList internally uses dynamic array to store the elements.
			2) Manipulation with ArrayList is slow because it internally uses array. If any element is removed from the array, all the bits are shifted in memory.
			3) ArrayList class can act as a list only because it implements List only.
			4) ArrayList is better for storing and accessing data.
			
		LinkedList :
			1) LinkedList internally uses doubly linked list to store the elements.
			2) Manipulation with LinkedList is faster than ArrayList because it uses doubly linked list so no bit shifting is required in memory.
			3) LinkedList class can act as a list and queue both because it implements List and Deque interfaces.
			4) LinkedList is better for manipulating data.
	
	
	
	
	*/

	public static void main(String[] args) {
		demo1();
	}

	private static void demo1() {
		
		long startTime = System.currentTimeMillis();
		
		ArrayList<String> arrlistobj = new ArrayList<String>();
        arrlistobj.add("0. Practice.GeeksforGeeks.org");
        arrlistobj.add("1. Quiz.GeeksforGeeks.org");
        arrlistobj.add("2. Code.GeeksforGeeks.org");
        arrlistobj.remove(1);  // Remove value at index 2
        System.out.println("ArrayList object output :" +  arrlistobj);
 
        // Checking if an element is present.
        if (arrlistobj.contains("2. Code.GeeksforGeeks.org"))
            System.out.println("Found");
        else
            System.out.println("Not found");
        
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println("Total Execution time for arraylist : "+totalTime);
 
 
        long startTime1 = System.currentTimeMillis();
        
        LinkedList llobj = new LinkedList();
        llobj.add("0. Practice.GeeksforGeeks.org");
        llobj.add("1. Quiz.GeeksforGeeks.org");
        llobj.add("2. Code.GeeksforGeeks.org");
        llobj.remove("1. Quiz.GeeksforGeeks.org");
        System.out.println("LinkedList object output :" + llobj);
 
        // Checking if an element is present.
        if (llobj.contains("2. Code.GeeksforGeeks.org"))
            System.out.println("Found");
        else
            System.out.println("Not found");
        
        long endTime1   = System.currentTimeMillis();
        long totalTime1 = endTime1 - startTime1;
        System.out.println("Total Execution time for LinkedList : "+totalTime1);
		
	}


}
