package com.basic.variables;

public class FinalVariables {
	
	public static void main(String[] args) {
		
		final int a = 10 ;
		
		// a=20;  Here is error. Enable to reassign.
		
		final Test1 t = new Test1();
		t.i = 30;
		
		// t = new Test(); Here is error. Enable to reassign.
		
	}

}


class Test1{
	int i = 10;
}
