package com.collection.list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class VectorDemo {
	
	/*
	
	The Vector class implements a growable array of objects. Vectors basically falls in legacy classes but now it is fully compatible with collections.

		- Vector implements a dynamic array that means it can grow or shrink as required. Like an array, 
		  it contains components that can be accessed using an integer index
		
		- They are very similar to ArrayList but Vector is synchronised and have some legacy method which 
		  collection framework does not contain.
		
		- It extends AbstractList and implements List interfaces.
	
	Important points regarding Increment of vector capacity: 
		If increment is specified, Vector will expand according to it in each allocation cycle but if increment 
		is not specified then vector�s capacity get doubled in each allocation cycle. Vector defines three protected data member:
		
		- int capacityIncreament: Contains the increment value.
		- int elementCount: Number of elements currently in vector stored in it.
		- Object elementData[]: Array that holds the vector is stored in it.
	
	
	*/

	public static void main(String[] args) {
		
		add();
		cloneMethod();
		ensureCapacity();
		trimToSize();
		setSize();
		retainAll();

	}

	private static void retainAll() {
		
		/*
		
		This method retains only the elements in this Vector that are contained in the specified Collection.
		
		*/
		
		System.out.println("----------- retainAll -------------");
		Vector vec = new Vector(7);
        Vector vecretain = new Vector(4);
 
        // use add() method to add elements in the vector
        vec.add(1);
        vec.add(2);
        vec.add(3);
        vec.add(4);
        vec.add(5);
        vec.add(6);
        vec.add(7);
        vec.add(2);
       
        // this elements will be retained
        vecretain.add(5);
        vecretain.add(3);
        vecretain.add(2);
 
        System.out.println("Calling retainAll()");
        vec.retainAll(vecretain);
       
        // let us print all the elements available in vector
        System.out.println("Numbers after removal :- "); 
    
        Iterator itr = vec.iterator();
    
        while(itr.hasNext())
        {
         System.out.println(itr.next());
                
        }
        
        /*String str = "Geeks for Geeks Geeks Geeks";
        String [] strArr = str.split(" ");
        
        List strV = Arrays.asList(strArr);
        
        Vector v = new Vector();
        v.add("Geeks");
        v.add("for");

        Vector v2 = new Vector();
        v2.addAll(strV);
        
        v2.retainAll(v);
        
        System.out.println("Geeks no of times : "+v2.size());*/
        
        
		
		
		
	}

	private static void setSize() {

		/*
			This method sets the size
		*/
		
		System.out.println("----------- setSize -------------");
		// create default vector of capacity 10
        Vector v = new Vector();
        
        v.add(1);
        v.add(2);
        v.add("Geeks");
        v.add("forGeeks");
        v.add(4);
         
        // setting new size of vector
        v.setSize(13);
         
        // size of vector
        System.out.println("size of vector: " + v.size());
		
	}

	private static void trimToSize() {
		
		/*
		
		This method trims the capacity of this vector to be the vector�s current size.
		
		*/
		
		System.out.println("----------- trimToSize -------------");
		
		// create default vector of capacity 10
        Vector v = new Vector();
        
        v.add(1);
        v.add(2);
        v.add("third");
        v.add("Fourth");
        v.add(4);
        
       // checking initial capacity
       System.out.println("Initial capacity: " + v.capacity());
        
       // trim capacity to size
       v.trimToSize();
        
       // checking capacity after triming
      System.out.println("capacity after triming: " + v.capacity());
	}

	private static void ensureCapacity() {

		/*
		
		This method increases the capacity of this vector, if necessary, to ensure that it can hold 
		at least the number of components specified by the minimum capacity argument .
		
		*/
		
		System.out.println("----------- ensureCapacity -------------");
		
		// create default vector of capacity 10
        Vector v = new Vector();
        
        // ensuring capacity
        v.ensureCapacity(22);
         
        // cheking capacity
        System.out.println("Minimum capacity: " + v.capacity());
		
	}

	private static void cloneMethod() {
		System.out.println("----------- clone -------------");
		
		// create default vector
        Vector v = new Vector();
        
        Vector v_clone = new Vector();
         
        v.add(0, 1);
        v.add(1, 2);
        v.add(2, "third");
        v.add(3, "Fourth");
        v.add(4, 3);
       
        v_clone = (Vector)v.clone();
         
        // checking vector
        System.out.println("Clone of v: " + v_clone);
	}

	private static void add() {
		System.out.println("----------- Add -------------");
		
		// create default vector
        Vector v = new Vector(10);
         
        v.add(0);
        v.add(1);
        v.add("Second");
        v.add("Third");
        v.add(4);
        v.add(5, "Fift");
        
        ArrayList arr = new ArrayList();
        arr.add(6);
        arr.add("Seventh");
        arr.add("Eighth");
        arr.add(9);
        
        v.addAll(6, arr);
        
        System.out.println("Vector is " + v);
	}

}
