package com.collection.set;

import java.util.EnumSet;

public class EnumSetDemo {
	
	/*
	
	EnumSet is one of the specialized implementation of Set interface for an enumeration type. 
	It extends AbstractSet and implements Set Interface in Java.

	It is a generic class declared as:
	
	public abstract class EnumSet<E extends Enum<E>> 
	
	Here, E specifies the elements. E must extend Enum, which enforces the requirement that the elements 
	must be of specified enum type.
	
	Important:
	
		- EnumSet class is a member of the Java Collections Framework & is not synchronized.
		- It�s a high performance set implementation, much faster than HashSet.
		- All elements of each EnumSet instance must be elements of a single enum type.
		
	*/

	public static void main(String[] args) {

		// create a set
        EnumSet<Gfg> set1, set2, set3, set4;
 
        // add elements
        set1 = EnumSet.of(Gfg.QUIZ, Gfg.CONTRIBUTE, Gfg.LEARN, Gfg.CODE);
        set2 = EnumSet.complementOf(set1);
        set3 = EnumSet.allOf(Gfg.class);
        set4 = EnumSet.range(Gfg.CODE, Gfg.CONTRIBUTE);
        System.out.println("Set 1: " + set1);
        System.out.println("Set 2: " + set2);
        System.out.println("Set 3: " + set3);
        System.out.println("Set 4: " + set4);
		
	}

}

enum Gfg 
{
    CODE, LEARN, CONTRIBUTE, QUIZ, MCQ
};
