package com.interfaces_and_abstract_classes.interfaces.nested_interface;

public class MainClassForInterface {

	public static void main(String[] args) {
		TestInterface.inner_interface_1 interface_1;
	    TestingForInterface t = new TestingForInterface();
	    interface_1 = t;
	    interface_1.show();
	    
	    TestInterface.inner_interface_3.display();
	}

}
