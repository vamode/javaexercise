package com.designpattern.creational.abstractfactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AbstractFactoryPatternExample {
	public static void main(String[] args) throws IOException{
		
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));  
		  
	    System.out.print("Enter the name of Bank from where you want to take loan amount: ");  
	    String bankName=br.readLine();  
	  
		System.out.print("\n");  
		System.out.print("Enter the type of loan e.g. home loan or business loan or education loan : ");  
		  
		String loanName=br.readLine();  
		AbstractFactory bankFactory = FactoryCreator.getFactory("Bank");  
		Bank b=bankFactory.getBank(bankName);  
		  
		System.out.print("\n");  
		System.out.print("Enter the interest rate for "+b.getBankName()+ ": ");  
		  
		double rate=Double.parseDouble(br.readLine());  
		System.out.print("\n");  
		System.out.print("Enter the loan amount you want to take: ");  
		  
		double loanAmount=Double.parseDouble(br.readLine());  
		System.out.print("\n");  
		System.out.print("Enter the number of years to pay your entire loan amount: ");  
		int years=Integer.parseInt(br.readLine());  
		  
		System.out.print("\n");  
		System.out.println("you are taking the loan from "+ b.getBankName());  
		  
		AbstractFactory loanFactory = FactoryCreator.getFactory("Loan");  
		           Loan l=loanFactory.getLoan(loanName);  
		           l.getInterestRate(rate);  
		           l.caluculateLoanPayment(loanAmount,years);
		
	}
	
	
	
	/*
	 * 
	OUTPUT
	
	Enter the name of Bank from where you want to take loan amount: sbi

	Enter the type of loan e.g. home loan or business loan or education loan : home

	Enter the interest rate for SBI BANK: 8.40

	Enter the loan amount you want to take: 1900000

	Enter the number of years to pay your entire loan amount: 15

	you are taking the loan from SBI BANK
	your monthly EMI is 18598.846016494586 for the amount 1900000.0 you have borrowed
	
	*/
	
}
