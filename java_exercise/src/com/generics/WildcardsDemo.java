package com.generics;

import java.util.Arrays;
import java.util.List;

public class WildcardsDemo {
	
	/*
	
	The question mark (?) is known as the wildcard in generic programming . It represents an unknown type. 
	The wildcard can be used in a variety of situations such as the type of a parameter, field, or local variable; 
	sometimes as a return type. Unlike arrays, different instantiations of a generic type are not compatible with each other, 
	not even explicitly. This incompatibility may be softened by the wildcard if ? is used as an actual type parameter.
	
	Types of wildcards in Java:
	
	1) Upper Bounded Wildcards 2) Lower Bounded Wildcards 3) Unbounded Wildcard
	
	*/

	public static void main(String[] args) {
		
		upperBoundedWildcards();
		lowerBoundedWildcards();
		unboundedWildcard();

	}

	private static void upperBoundedWildcards() {
		
		/*
		
		These wildcards can be used when you want to relax the restrictions on a variable. For example, 
		say you want to write a method that works on List < integer >, List < double >, and List < number > , 
		you can do this  using an upper bounded wildcard.
		
		To declare an upper-bounded wildcard, use the wildcard character (�?�), followed by the extends keyword, 
		followed by its upper bound.
		
		public static void add(List<? extends Number> list)
		
		*/
		
		//Upper Bounded Integer List
        List<Integer> list1= Arrays.asList(4,5,6,7);
         
        //printing the sum of elements in list
        System.out.println("Total sum is:"+sum(list1));
 
        //Double list
        List<Double> list2=Arrays.asList(4.1,5.1,6.1);
         
        //printing the sum of elements in list
        System.out.println("Total sum is:"+sum(list2));
		
	}
	
	private static double sum(List<? extends Number> list) 
    {
        double sum=0.0;
        for (Number i: list)
        {
            sum+=i.doubleValue();
        }
 
        return sum;
    }

	private static void lowerBoundedWildcards() {
		
		/*
		
		It is expressed using the wildcard character (�?�), followed by the super keyword, followed by its lower bound: <? super A>.
 			Syntax: Collectiontype <? super A>
 			
 		Here arguments can be Integer or superclass of Integer(which is Number). The method printOnlyIntegerClassorSuperClass 
 		will only take Integer or its superclass objects. However if we pass list of type Double then we will get 
 		compilation error. It is because only the Integer field or its superclass can be passed . 
 		Double is not the superclass of Integer.

		Use extend wildcard when you want to get values out of a structure and super wildcard when you put 
		values in a structure. Don�t use wildcard when you get and put values in a structure.

		Note: You can specify an upper bound for a wildcard, or you can specify a lower bound, but you cannot specify both.
		
		*/
		
		//Lower Bounded Integer List
        List<Integer> list1= Arrays.asList(4,5,6,7);
         
        //Integer list object is being passed
        printOnlyIntegerClassorSuperClass(list1);
 
        //Number list
        List<Number> list2= Arrays.asList(8,9,10,11);
         
        //Integer list object is being passed
        printOnlyIntegerClassorSuperClass(list2);
		
	}
	
	public static void printOnlyIntegerClassorSuperClass(List<? super Integer> list)
    {
        System.out.println("lowerBoundedWildcards : "+list);
    }

	private static void unboundedWildcard() {
		
		/*
		
		This wildcard type is specified using the wildcard character (?), for example, List. 
		This is called a list of unknown type. These are useful in the following cases
		
			1) When writing a method which can be employed using functionality provided in Object class.
			2) When the code is using methods in the generic class that don�t depend on the type parameter
		
		*/
		
		//Integer List
        List<Integer> list1= Arrays.asList(1,2,3);
 
        //Double list
        List<Double> list2=Arrays.asList(1.1,2.2,3.3);
 
        printlist(list1);
 
        printlist(list2);
		
	}
	
	private static void printlist(List<?> list) 
    {
 
        System.out.println("unboundedWildcard : "+list);
    }

}
