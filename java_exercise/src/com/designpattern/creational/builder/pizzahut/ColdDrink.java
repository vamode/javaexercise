package com.designpattern.creational.builder.pizzahut;

public abstract class ColdDrink implements Item{
	@Override
	public abstract float price();
}
