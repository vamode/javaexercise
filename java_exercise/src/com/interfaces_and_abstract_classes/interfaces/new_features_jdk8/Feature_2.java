package com.interfaces_and_abstract_classes.interfaces.new_features_jdk8;

public interface Feature_2 {
	
	/*
	
	- Another feature that was added in JDK 8 is that we can now define static methods in interfaces 
	  which can be called independently without an object. Note: these methods are not inherited.
	
	*/
	
	final int a = 10;
    static void display()
    {
        System.out.println("static display method called from Feature_2 interface...");
    }

}
