package com.collection.list;

import java.util.ArrayList;
import java.util.Iterator;

public class ArrayListDemo {
	
	/*
	
	ArrayList is a part of collection framework and is present in java.util package. 
	It provides us dynamic arrays in Java. Though, it may be slower than standard arrays 
	but can be helpful in programs where lots of manipulation in the array is needed.
	
	
	- ArrayList inherits AbstractList class and implements List interface.
	
	- ArrayList is initialized by a size, however the size can increase if collection grows or shrunk 
	  if objects are removed from the collection.
	
	- Java ArrayList allows us to randomly access the list.
	
	- ArrayList can not be used for primitive types, like int, char, etc. We need a wrapper class for 
	  such cases.
	
	- ArrayList in Java can be seen as similar to vector in C++.
	
	- Java ArrayList class can contain duplicate elements.
	
	- Java ArrayList class maintains insertion order.
	
	- Java ArrayList class is non synchronized.
	
	- Java ArrayList allows random access because array works at the index basis.
	
	- In Java ArrayList class, manipulation is slow because a lot of shifting needs to be occurred if any element is removed from the array list.
	
	
	ArrayList class declaration :
		- public class ArrayList<E> extends AbstractList<E> implements List<E>, RandomAccess, Cloneable, Serializable
	
	Java Non-generic Vs Generic Collection : 
		- Java collection framework was non-generic before JDK 1.5. Since 1.5, it is generic.
		- Java new generic collection allows you to have only one type of object in collection. Now it is type safe so typecasting is not required at run time.
		
			Non-Generic : ArrayList al=new ArrayList();//creating old non-generic arraylist  
			Generic     : ArrayList<String> al=new ArrayList<String>();//creating new generic arraylist
	
	
	
	*/

	public static void main(String[] args) {
		
		/*
		
		There are two ways to traverse collection elements:
			1. By Iterator interface.
			2. By for-each loop.
		
		*/
		arrayListExample();
		
		retainAll();
		 

	}
	
	private static void retainAll() {
		
		/*
		
		This method returns a boolean value True/False.

		This method is used to retain the elements of one list into another. 
		In other word we can say, it deletes all those elements of current list collection which do not exist in specified collection object. 
		It copies those elements which are equal in both list collection objects and deletes rest of the other elements from the specified list. 
		This method returns true if the list has changed after the call, otherwise false.
		
		*/

		System.out.println("-----retainAll------");
		
	  ArrayList<String> al=new ArrayList<String>();  
	  al.add("Ravi");  
	  al.add("Vijay");  
	  al.add("Ajay");  
	  al.add("Vishal");
	  
	  ArrayList<String> al2=new ArrayList<String>();  
	  al2.add("Ravi");  
	  al2.add("Hanumat");
	  al2.add("Ajay");
	  al.retainAll(al2);  
	  
	  System.out.println("iterating the elements after retaining the elements of al2..."); 
	  
	  Iterator itr=al.iterator();  
	  
	  while(itr.hasNext()){  
	   System.out.println(itr.next());  
	  }
		
	}

	private static void arrayListExample() {
		
		ArrayList<String> list = new ArrayList<String>();// Creating arraylist
		list.add("Ravi");// Adding object in arraylist
		list.add("Vijay");
		list.add("Ravi");
		list.add("Ajay");
		
		// Traversing list through Iterator
		System.out.println("-----Using Iterator------");
		Iterator itr = list.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}
		
		System.out.println("-----Using For------");
		for(String obj:list) {
			System.out.println(obj); 
		}
		
	}

}
