package com.collection.iterators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class IteratorDemo {
	
	/*
	
	
	It is a universal iterator as we can apply it to any Collection object. 
	By using Iterator, we can perform both read and remove operations. 
	It is improved version of Enumeration with additional functionality of remove-ability of a element.
	
	Iterator must be used whenever we want to enumerate elements in all Collection framework implemented interfaces 
	like Set, List, Queue, Deque and also in all implemented classes of Map interface.
	
	
	remove() method can throw two exceptions :

		- UnsupportedOperationException : If the remove operation is not supported by this iterator
		
		- IllegalStateException : If the next method has not yet been called, 
		or the remove method has already been called after the last call to the next method
		
	
	Limitations of Iterator :

		- Only forward direction iterating is possible.
		- Replacement and addition of new element is not supported by Iterator.
	
	
	
	*/
	
	public static void main(String[] args) {
		
		ArrayList al = new ArrayList();
		 
        for (int i = 0; i < 10; i++)
            al.add(i);
 
        System.out.println(al);
 
        // at beginning itr(cursor) will point to
        // index just before the first element in al
        Iterator itr = al.iterator();
 
        // checking the next element availabilty
        while (itr.hasNext())
        {
            //  moving cursor to next element
            int i = (Integer)itr.next();
 
            // getting even elements one by one
            System.out.print(i + " ");
 
            // Removing odd elements
            if (i % 2 != 0)
               itr.remove(); 
        }
        System.out.println(); 
        System.out.println(al);
        
        
        /* ----------------------HashMap--------------------------- */
        
        
     // Creating a HashMap of int keys and String values
        HashMap<Integer, String> hashmap = new HashMap<Integer, String>();
     
        // Adding Key and Value pairs to HashMap
        hashmap.put(11,"Value1");
        hashmap.put(22,"Value2");
        hashmap.put(33,"Value3");
        hashmap.put(44,"Value4");
        hashmap.put(55,"Value5");
     
        // Getting a Set of Key-value pairs
        Set entrySet = hashmap.entrySet();
     
        // Obtaining an iterator for the entry set
        Iterator it = entrySet.iterator();
     
        // Iterate through HashMap entries(Key-Value pairs)
        System.out.println("HashMap Key-Value Pairs : ");
        while(it.hasNext()){
           Map.Entry me = (Map.Entry)it.next();
           System.out.println("Key is: "+me.getKey() + 
           " & " + 
           " value is: "+me.getValue());
           
           if("22".equals(me.getKey().toString())){
        	   it.remove();
           }
       }
        
        Iterator it1 = entrySet.iterator();
        while(it1.hasNext()){
            Map.Entry me = (Map.Entry)it1.next();
            System.out.println("After Remove 22 : Key is: "+me.getKey() + 
            " & " + 
            " value is: "+me.getValue());
        }
        
	}

}
