package com.designpattern.creational.prototype;

public class CloneableInterfaceDemo implements Cloneable{
	
	private int rollno;  
	private String name;
	
	/**
	 * @param rollno
	 * @param name
	 */
	public CloneableInterfaceDemo(int rollno, String name) {
		super();
		this.rollno = rollno;
		this.name = name;
	} 
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
	
	

}
