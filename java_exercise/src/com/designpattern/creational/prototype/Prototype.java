package com.designpattern.creational.prototype;

public interface Prototype {
	public Prototype getClone();
}
