package com.generics;

import java.util.ArrayList;

public class GenericDemo <T,X>{
	
	/*
	
	we use <> to specify parameter types in generic class creation. To create objects of generic class, we use following syntax.
	
	// To create an instance of generic class 
		BaseType <Type> obj = new BaseType <Type>()
	
	Note: In Parameter type we can not use primitives like 
	      'int','char' or 'double'.
	
	We can also pass multiple Type parameters in Generic classes.
	
	*/
	
	private T objT;
	private X objX;
	
	public GenericDemo(T objT,X objX) {
		this.objT = objT;
		this.objX = objX;
	}
	
	public T getObjectT(){ 
		return this.objT; 
	}
	
	public X getObjectX(){ 
		return this.objX; 
	}

	public static void main(String[] args) {
		
		System.out.println("---------- Generic Class---------------");
		
		// instance of Integer type
		GenericDemo <Integer,String> iObj = new GenericDemo<Integer,String>(15,"Vishal Amode");
        System.out.println("iObj T : "+iObj.getObjectT());
        System.out.println("iObj X : "+iObj.getObjectX());
  
        // instance of String type
        GenericDemo <String,Integer> sObj =
                          new GenericDemo<String,Integer>("Sidh Amode",20);
        System.out.println("sObj T : "+sObj.getObjectT());
        System.out.println("sObj X : "+sObj.getObjectX());
        
        System.out.println("---------- Generic Function---------------");
        
        // Calling generic method with Integer argument
        genericDisplay(11);
  
        // Calling generic method with String argument
        genericDisplay("Siddhant..");
  
        // Calling generic method with double argument
        genericDisplay(1.0);
        
        System.out.println("---------- Advantages of Generics ---------------");
        
        advantagesOfGenerics();

	}
	
	// A Generic method example
    static <T> void genericDisplay (T element)
    {
    	/*
    	 
    	We can also write generic functions that can be called with different types of arguments based on 
    	the type of arguments passed to generic method, the compiler handles each method.
    	
    	*/
    	
        System.out.println(element.getClass().getName() +
                           " = " + element);
    }
    
    private static void advantagesOfGenerics() {

    	/*
    	
    	1) Code Reuse: We can write a method/class/interface once and use for any type we want.
    	
    	2) Type Safety : Generics make errors to appear compile time than at run time 
    	   (It�s always better to know problems in your code at compile time rather than 
    	   making your code fail at run time). Suppose you want to create an ArrayList that 
    	   store name of students and if by mistake programmer adds an integer object instead 
    	   of string, compiler allows it. But, when we retrieve this data from ArrayList, it 
    	   causes problems at runtime.
    	
    	3) Individual Type Casting is not needed: If we do not use generics, then, in the above 
    	   example every-time we retrieve data from ArrayList, we have to typecast it. Typecasting 
    	   at every retrieval operation is a big headache. If we already know that our list only holds 
    	   string data then we need not to typecast it every time.
    	
    	4) Implementing generic algorithms: By using generics, we can implement algorithms that work on 
    	   different types of objects and at the same they are type safe too.
    	
    	*/
    	
    	
    	// Creating a an ArrayList with String specified
        ArrayList <String> al = new ArrayList<String>();
 
        al.add("Sachin");
        al.add("Rahul");
 
        // Now Compiler doesn't allow this
        // al.add(10); 
 
        // Typecasting is not needed 
        String s1 = al.get(0);
        String s2 = al.get(1);
        String s3 = (String)al.get(2);
    	
	}

}
