package com.interfaces_and_abstract_classes.interfaces.nested_interface;

public interface TestInterface {
	
	/*
	
	An interface can be declared inside another interface also. 
	We mention the interface as i_name1.i_name2 where i_name1 is the name of the interface in 
	which it is nested and i_name2 is the name of the interface to be implemented.
	
	*/
	interface inner_interface_1{
		 void show();
	}
	
	/*
	
	In the above example, access specifier is public even if we have not written public. 
	If we try to change access specifier of interface to anything other than public, we get compiler error. 
	Remember, interface members can only be public..
	
	protected interface inner_interface_2{
		void show();
	}
	
	
	*/
	
	static interface inner_interface_3{
		 static void display(){
			 System.out.println("static view method of static inner_interface_3..");
		 }
		 
		 /*
		 
		 We can not have non-static method in static innner interface.
		 
		 public void export(){
			 System.out.println("static view method of static inner_interface_3..");
		 }
		 
		 */
		 
		 
	}
	

}
