package com.collection.iterators;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class EnumerationDemo {
	
	/*
	
		Enumeration is a interface used to get elements of legacy collections(Vector, Hashtable).
		Enumeration is the first iterator present from JDK 1.0, rests are included in JDK 1.2 with more functionality. 
		Enumerations are also used to specify the input streams to a SequenceInputStream. 
		We can create Enumeration object by calling elements() method of vector class on any vector object
		
		Limitations of Enumeration :

		- Enumeration is for legacy classes(Vector, Hashtable) only. Hence it is not a universal iterator.
		- Remove operations can�t be performed using Enumeration.
		- Only forward direction iterating is possible.
	
	
	*/

	public static void main(String[] args) {

		System.out.println("-------------------Vector----------------------\n");
		// Create a vector and print its contents
        Vector v = new Vector();
        for (int i = 0; i < 10; i++)
            v.addElement(i);
        System.out.println(v);
 
        // At beginning e(cursor) will point to
        // index just before the first element in v
        Enumeration e = v.elements();
 
        // Checking the next element availability
        while (e.hasMoreElements())
        {
            // moving cursor to next element
            int i = (Integer)e.nextElement();
 
            System.out.print(i + " ");
        }
        
        System.out.println("\n-----------------HashTable---------------------\n");
        
        Hashtable<String, String> hm = new Hashtable<String, String>();
        //add key-value pair to Hashtable
        hm.put("first", "FIRST INSERTED");
        hm.put("second", "SECOND INSERTED");
        hm.put("third","THIRD INSERTED");
        Enumeration<String> keys = hm.keys();
        while(keys.hasMoreElements()){
            String key = keys.nextElement();
            System.out.println("Value of "+key+" is: "+hm.get(key));
        }
        
        
		
		
	}

}
