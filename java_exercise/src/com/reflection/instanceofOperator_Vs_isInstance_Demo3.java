package com.reflection;

public class instanceofOperator_Vs_isInstance_Demo3 {

	//instanceof operator throws compile time error(Incompatible conditional operand types) if we check object with other 
	//classes which it doesn�t instantiate.
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Integer i = new Integer(5);
		 
        // Below line causes compile time error:-
        // Incompatible conditional operand types
        // Integer and String
		
       /* System.out.println(i instanceof String);*/

	}

}
