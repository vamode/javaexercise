package com.reflection;

public class instanceofOperator_Vs_isInstance_Demo1 {

	 public static void main(String[] args) {
	        Integer i = new Integer(5);
	 
	        // prints true as i is instance of class
	        // Integer
	        System.out.println(i instanceof Integer);
	 }
	
}
