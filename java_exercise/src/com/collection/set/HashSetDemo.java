package com.collection.set;

import java.util.HashSet;
import java.util.Iterator;

public class HashSetDemo {
	
	/*
	
	- Implements Set Interface.
	- Underlying data structure for HashSet is hashtable.
	- As it implements the Set Interface, duplicate values are not allowed.
	- Objects that you insert in HashSet are not guaranteed to be inserted in same order. Objects are inserted based on their hash code.
	- NULL elements are allowed in HashSet.
	- HashSet stores the elements by using a mechanism called hashing.
	- HashSet also implements Searlizable and Cloneable interfaces.
	- Default initial capacity is 16 and default load factor is 0.75.
	
								Number of stored elements in the table
			   load factor = -----------------------------------------
			                        Size of the hash table
			                        
				E.g. If internal capacity is 16 and load factor is 0.75 then, 
					 number of buckets will automatically get increased when table has 12 elements in it.
					 
	HashSet class declaration : 
	
	public class HashSet<E> extends AbstractSet<E> implements Set<E>, Cloneable, Serializable
	
	*/

	public static void main(String[] args) {

		sampleProgram();
		howInternallyWork();
		
	}

	private static void sampleProgram() {

		HashSet<String> h = new HashSet<String>();
		 
        // adding into HashSet
        h.add("India");
        h.add("Australia");
        h.add("South Africa");
        h.add("India");// adding duplicate elements
 
        // printing HashSet
        System.out.println(h);
        System.out.println("List contains India or not:" +
                           h.contains("India"));
 
        // Removing an item
        h.remove("Australia");
        System.out.println("List after removing Australia:"+h);
 
        // Iterating over hash set items
        System.out.println("Iterating over list:");
        Iterator<String> i = h.iterator();
        while (i.hasNext())
            System.out.println(i.next());
		
	}
	
	private static void howInternallyWork() {
		
		/*
		
		How HashSet internally work?
			All the classes of Set interface internally backed up by Map. HashSet uses HashMap for storing its object internally. 
		You must be wondering that to enter a value in HashMap we need a key-value pair, but in HashSet we are passing only one value.
		
		Then how is it storing in HashMap?
			Actually the value we insert in HashSet acts as key to the map Object and for its value java uses a constant variable. .
		So in key-value pair all the keys will have same value.
		
		
		public class HashSet<E>	extends AbstractSet<E> implements Set<E>, Cloneable, java.io.Serializable
		{
		    private transient HashMap<E,Object> map;
		    
		    // Dummy value to associate with an Object in the backing Map
		    
		    private static final Object PRESENT = new Object();
		    
		    public HashSet() {
		        map = new HashMap<>();
		    }
		    
		    // SOME CODE ,i.e Other methods in Hash Set
		    
		    
		    public boolean add(E e) {
		        return map.put(e, PRESENT)==null;
		    }
		    
		    // SOME CODE ,i.e Other methods in Hash Set
		}
		
		
		
		
				
		*/
	}

}
