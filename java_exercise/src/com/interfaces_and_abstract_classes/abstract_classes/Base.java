package com.interfaces_and_abstract_classes.abstract_classes;

public abstract class Base {
	
	/*
	
	- In Java, an instance of an abstract class cannot be created, we can have references of abstract class type though.
	
	- An abstract class can contain constructors in Java. And a constructor of abstract class is called when an instance 
	  of a inherited class is created. For example, the following is a valid Java program.
	  
	- We can have an abstract class without any abstract method. This allows us to create classes that cannot be instantiated, 
	  but can only be inherited.
	  
	- Abstract classes can also have final methods (methods that cannot be overridden).
    
    */
	
	Base() {
		System.out.println("Base Constructor Called");
	}
	
	abstract void fun(); // It is fine, if we will comment abstract method fun()
	
	final void funFinal() {
		System.out.println("Base funFinal() called");
	}
	
	public static void staticMethod(){
		System.out.println("Base staticMethod() called");
	}
	
	/*
	
	Exercise:
		1. Is it possible to create abstract and final class in Java?
			- No Because, because Abstract must be inherited and final can't.
			
		2. Is it possible to have an abstract method in a final class?
			- No Because, Abstract method don't have body and it must me implemented by its sub-class.
			  So, Final class can not have sub-class.
			   
		3. Is it possible to inherit from multiple abstract classes in Java?
			- We are unable to inherit multiple abstract classes in same class.
	
	*/			
				
	
	
}
