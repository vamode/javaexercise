package com.interfaces_and_abstract_classes.interfaces.nested_interface;

public class TestingForClass implements TestClass.inner_interface_1,TestClass.inner_interface_2{

	@Override
	public void show() {
		System.out.println("show method of inner_interface_1..");
	}

	@Override
	public void display() {
		System.out.println("display method of protected inner_interface_2..");
	}

}
