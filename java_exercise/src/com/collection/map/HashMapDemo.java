package com.collection.map;

import java.util.HashMap;
import java.util.Map;

public class HashMapDemo {
	
	/*
	
	HashMap is a part of collection in Java since 1.2. It provides the basic implementation of Map interface of Java. 
	It stores the data in (Key,Value) pairs. To access a value you must know its key, otherwise you can�t access it. 
	HashMap is known as HashMap because it uses a technique Hashing. Hashing is a technique of converting a large String 
	to small String that represents same String. A shorter value helps in indexing and faster searches. 
	HashSet also uses HashMap internally. It internally uses link list to store key-value pairs. We will know about HashSet 
	in detail in further articles.
	
	HashMap don�t allow duplicate keys, but allows duplicate values. That means A single key can�t contain more than 
	1 value but more than 1 key can contain a single value. HashMap allows null key also but only once and multiple null values. 
	This class makes no guarantees as to the order of the map; in particular, it does not guarantee that the order will remain 
	constant over time. It is roughly simillar to HashTable but is unsynchronized.
	
	
	Performance of HashMap :

	Performance of HashMap depends on 2 parameters:
	
		1. Initial Capacity
		2. Load Factor
	
	As already told capacity is simply the number of buckets where and Initial Capacity is the capacity of HashMap 
	instance when it is created. The Load Factor is a measure that when rehashing should be done. Rehashing is a process 
	of increasing the capacity. In HashMap capacity is multiplied by 2. Load Factor is also a measure that what fraction 
	of the HashMap is allowed to fill before rehashing. When the number of entries in HashMap increases the product of current
	capacity and load factor the capacity is increased that is rehashing is done. If we keep the initial capacity higher then 
	rehashing will never be done. But by keeping it higher it increases the time complexity of iteration. 
	So it should be choosed very cleverly to increase the performance. The expected number of values should be taken into account 
	to set initial capacity. Most generally preffered load factor value is 0.75 which provides a good deal between time and space 
	costs. Load factor�s value varies between 0 and 1.
	
	Synchronized HashMap : 
		Map m = Collections.synchronizedMap(new HashMap(...));
	
	*/

	public static void main(String[] args) {
		
		sampleProgram();
		howInternallyWork();
		

	}

	private static void sampleProgram() {
		
		HashMap<String, Integer> map = new HashMap<>();
        
        print(map);
        map.put("vishal", 10);
        map.put("sachin", 30);
        map.put("vaibhav", 20);
         
        System.out.println("Size of map is:- " + map.size());
     
        print(map);
        if (map.containsKey("vishal")) 
        {
            Integer a = map.get("vishal");
            System.out.println("value for key \"vishal\" is:- " + a);
        }
         
        map.clear();
        print(map);
		
	}
	
	public static void print(Map<String, Integer> map) 
    {
        if (map.isEmpty()) 
        {
            System.out.println("map is empty");
        }
         
        else
        {
            System.out.println(map);
        }
    }

	private static void howInternallyWork() {
		
	}

}
