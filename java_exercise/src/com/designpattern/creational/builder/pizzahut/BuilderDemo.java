package com.designpattern.creational.builder.pizzahut;

import java.io.IOException;

public class BuilderDemo {

	public static void main(String[] args) throws IOException {

		OrdereBuilder builder = new OrdereBuilder();
		
		OrderedItems orderedItems = builder.preparePizza();
		
		orderedItems.showItems();
		
		System.out.println("\n");  
        System.out.println("Total Cost : "+ orderedItems.getCost()); 
		
	}

}
