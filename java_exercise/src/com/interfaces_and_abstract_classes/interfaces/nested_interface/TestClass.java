package com.interfaces_and_abstract_classes.interfaces.nested_interface;

public class TestClass {
	
	/*
	
	Interfaces (or classes) can have only public and default access specifiers when declared outside any other class. 
	This interface declared in a class can either be default, public, private, protected. 
	While implementing the interface, we mention the interface as c_name.i_name where c_name is the name of the class 
	in which it is nested and i_name is the name of the interface itself.
	
	*/
	
	interface inner_interface_1{
		 void show();
	}
	
	
	/*
	
	The access specifier in above example is default. We can assign public, protected or private also. 
	Below is an example of protected. In this particular example, if we change access specifier to private, 
	we get compiler error because a derived class tries to access it.
	
	*/
	protected interface inner_interface_2{
		 void display();
	}
	
	private interface inner_interface_3{
		 static void view(){
			 System.out.println("static view method of private inner_interface_3 thorough public demo method from Test class..");
		 }
	}
	
	public void demo(){
		inner_interface_3.view();
	}
	
}
