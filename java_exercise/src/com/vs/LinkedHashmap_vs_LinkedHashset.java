package com.vs;

public class LinkedHashmap_vs_LinkedHashset {
	
	/*
	
	LinkedHashmap vs LinkedHashset

	LinkedHashMap does a mapping of keys to values whereas a LinkedHashSet simply stores a collection of things with no duplicates.
	LinkedHashMap extends HashMap and LinkedHashSet extends HashSet.
	
	Important : 
		Keeping the insertion order in both LinkedHashmap and LinkedHashset have additional associated costs, 
		both in terms of spending additional CPU cycles and needing more memory. If you do not need the insertion order maintained, 
		it is recommended to use the lighter-weight HashSet and HashMap instead.
		
	*/

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
