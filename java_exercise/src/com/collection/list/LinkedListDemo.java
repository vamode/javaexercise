package com.collection.list;

import java.util.Iterator;
import java.util.LinkedList;

public class LinkedListDemo {
	
	/*
	
	Java LinkedList class uses doubly linked list to store the elements. It provides a linked-list data structure. It inherits the AbstractList class and implements List and Deque interfaces.

	The important points about Java LinkedList are:
		- Java LinkedList class can contain duplicate elements.
		- Java LinkedList class maintains insertion order.
		- Java LinkedList class is non synchronized.
		- In Java LinkedList class, manipulation is fast because no shifting needs to be occurred.
		- Java LinkedList class can be used as list, stack or queue
	
	Doubly Linked List
		In case of doubly linked list, we can add or remove elements from both side.
		
	LinkedList class declaration : 
		public class LinkedList<E> extends AbstractSequentialList<E> implements List<E>, Deque<E>, Cloneable, Serializable
	
	
	
	
	*/

	public static void main(String[] args) {
		
		demo1();
		
		demo2();

	}

	private static void demo1() {
		
		System.out.println("-----Demo 1------");

		LinkedList<String> al = new LinkedList<String>();
		al.add("Ravi");
		al.add("Vijay");
		al.add("Ravi");
		al.add("Ajay");

		Iterator<String> itr = al.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}
		
	}
	
	private static void demo2() {
		
		System.out.println("-----Demo 2------");

		// Creating object of class linked list
		LinkedList<String> object = new LinkedList<String>();

		// Adding elements to the linked list
		object.add("A");
		object.add("B");
		object.addLast("C");
		object.addFirst("D");
		object.add(2, "E");
		object.add("F");
		object.add("G");
		System.out.println("Linked list : " + object);

		// Removing elements from the linked list
		object.remove("B");
		object.remove(3);
		object.removeFirst();
		object.removeLast();
		System.out.println("Linked list after deletion: " + object);

		// Finding elements in the linked list
		boolean status = object.contains("E");

		if (status)
			System.out.println("List contains the element 'E' ");
		else
			System.out.println("List doesn't contain the element 'E'");

		// Number of elements in the linked list
		int size = object.size();
		System.out.println("Size of linked list = " + size);

		// Get and set elements from linked list
		Object element = object.get(2);
		System.out.println("Element returned by get() : " + element);
		object.set(2, "Y");
		System.out.println("Linked list after change : " + object);

	}

}
