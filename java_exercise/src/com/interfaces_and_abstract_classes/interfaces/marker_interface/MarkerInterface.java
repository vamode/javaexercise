package com.interfaces_and_abstract_classes.interfaces.marker_interface;

public interface MarkerInterface {
	
	/*
	
	It is an empty interface (no field or methods). Examples of marker interface are Serializable, Clonnable and Remote interface. 
	All these interfaces are empty interfaces.

	*/
	
}
