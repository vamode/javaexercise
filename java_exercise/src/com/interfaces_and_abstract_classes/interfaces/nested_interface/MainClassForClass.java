package com.interfaces_and_abstract_classes.interfaces.nested_interface;

public class MainClassForClass {

	public static void main(String[] args) {

		TestClass.inner_interface_1 interface_1;
		TestingForClass testingForClass = new TestingForClass();
		interface_1 = testingForClass;
		interface_1.show();
		
		System.out.println("----------------------------------------------------------------------------");
		
		TestClass.inner_interface_2 interface_2;
        TestingForClass t = new TestingForClass();
        interface_2=t;
        interface_2.display();
        
        System.out.println("----------------------------------------------------------------------------");
        
        TestClass testClass = new TestClass();
        testClass.demo();
	}

}
