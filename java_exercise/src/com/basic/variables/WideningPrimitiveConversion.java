package com.basic.variables;

public class WideningPrimitiveConversion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.print("1 : "+"Y" + "O");
		System.out.println("\n");
		
		System.out.print("2 : "+'L');
		System.out.println("\n");
		
		System.out.print("3 : "+'O');
		System.out.println("\n");
		
        System.out.print("4 : "+'L' + 'O');
        System.out.println("\n");
        
        System.out.print("5 : "+"2" + "3");
        System.out.println("\n");
        
        System.out.print("6 : "+'4');
        System.out.println("\n");
        
        System.out.print("7 : "+'5');
        System.out.println("\n");
        
        System.out.print("8 : "+'4' + '5');
        System.out.println("\n");
        
        System.out.print("9 : "+'4' * '5');
        System.out.println("\n");
        
        System.out.print("10 : "+'8' / '2');

	}

}
