package com.designpattern.creational.builder.pizzahut;

public class ExtraLargeNonVegPizza extends NonVegPizza{

	@Override
	public String name() {
		return "Non-Veg Pizza";
	}

	@Override
	public String size() {
		return "Extra-Large Size";
	}

	@Override
	public float price() {
		return 250.0f;
	}

}
