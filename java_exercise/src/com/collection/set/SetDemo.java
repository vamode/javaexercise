package com.collection.set;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetDemo {
	
	/*
	
	- Set is an interface which extends Collection. It is an unordered collection of objects 
	  in which duplicate values cannot be stored.
	- Basically, Set is implemented by HashSet, LinkedSet or TreeSet (sorted representation).
	- Set has various methods to add, remove clear, size, etc to enhance the usage of this interface
	
	*/

	public static void main(String[] args) {
		
		demo1();
		demo2();

	}

	private static void demo1() {
		
		// Set deonstration using HashSet
        Set<String> hash_Set = new HashSet<String>();
        hash_Set.add("Geeks");
        hash_Set.add("For");
        hash_Set.add("Geeks");
        hash_Set.add("Example");
        hash_Set.add("Set");
        System.out.print("Set output without the duplicates");
 
        System.out.println(hash_Set);
 
        // Set deonstration using TreeSet
        System.out.print("Sorted Set after passing into TreeSet");
        Set<String> tree_Set = new TreeSet<String>(hash_Set);
        System.out.println(tree_Set);
        
        /*
        	(Please note that we have entered a duplicate entity but it is not displayed in the output. Also, 
        	 we can directly sort the entries by passing the unordered Set in as the parameter of TreeSet)
        */
		
	}

	private static void demo2() {
		
		/*
		
		Let�s take an example of two integer Sets:

		[1, 3, 2, 4, 8, 9, 0]
		[1, 3, 7, 5, 4, 0, 7, 5]
		
		Union :
			In this, we could simply add one Set with other. Since the Set will itself not allow any duplicate entries, 
			we need not take care of the common values.
		
		Intersection :
			We just need to retain the common values from both Sets.
			
		Difference :
			We just need to remove tall the values of one Set from the other.
		
		*/
		
		Set<Integer> a = new HashSet<Integer>();
        a.addAll(Arrays.asList(new Integer[] {1, 3, 2, 4, 8, 9, 0}));
        Set<Integer> b = new HashSet<Integer>();
        b.addAll(Arrays.asList(new Integer[] {1, 3, 7, 5, 4, 0, 7, 5}));
 
        // To find union
        Set<Integer> union = new HashSet<Integer>(a);
        union.addAll(b);
        System.out.print("Union of the two Set");
        System.out.println(union);
 
        // To find intersection
        Set<Integer> intersection = new HashSet<Integer>(a);
        intersection.retainAll(b);
        System.out.print("Intersection of the two Set");
        System.out.println(intersection);
 
        // To find the symmetric difference
        Set<Integer> difference = new HashSet<Integer>(a);
        difference.removeAll(b);
        System.out.print("Difference of the two Set");
        System.out.println(difference);
		
	}

}
