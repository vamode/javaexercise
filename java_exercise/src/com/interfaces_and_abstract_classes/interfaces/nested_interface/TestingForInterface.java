package com.interfaces_and_abstract_classes.interfaces.nested_interface;

public class TestingForInterface implements TestInterface.inner_interface_1{

	@Override
	public void show() {
		System.out.println("show method of inner_interface_1");
	}

}
