package com.interfaces_and_abstract_classes.interfaces.new_features_jdk8;

public interface Feature_1 {
	
	/*
	
	- Prior to JDK 8, interface could not define implementation. We can now add default implementation for interface methods. 
	  This default implementation has special use and does not affect the intention behind interfaces.
	
	- Suppose we need to add a new function in an existing interface. Obviously the old code will not work as the 
	  classes have not implemented those new functions. So with the help of default implementation, we will give a 
	  default body for the newly added functions. Then the old codes will still work.
	
	*/

	final int a = 10;
    default void display()
    {
    	System.out.println("display method called from Feature_1 interface...");
    }
	
}
