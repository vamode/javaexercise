package com.designpattern.creational.factory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GenerateBill {

	public static void main(String[] args) throws IOException{

		
		GetPlanFactory factory = new GetPlanFactory();
		
		System.out.print("Enter the name of plan for which the bill will be generated: ");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		String planName = reader.readLine();
		
		System.out.print("Enter the number of units for bill will be calculated: ");  
		int units = Integer.parseInt(reader.readLine());
		
		Plan plan = factory.getPlan(planName);
		
		plan.getRate();
		plan.calculateBill(units);
		
		
		/*
		
		OUTPUT :
		
		Enter the name of plan for which the bill will be generated: COMMERCIALPLAN
		Enter the number of units for bill will be calculated: 80
		Bill : 600.0
		
		*/
		
	}

}
