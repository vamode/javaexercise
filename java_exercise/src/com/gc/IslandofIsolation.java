package com.gc;

public class IslandofIsolation {
	
	private IslandofIsolation i;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		IslandofIsolation i1 = new IslandofIsolation();
		IslandofIsolation i2 = new IslandofIsolation();
		
		 // Object of i1 gets a copy of i2
		i1.i = i2;
		
		// Object of i2 gets a copy of i1
		i2.i = i1;
		
		// Till now no object eligible
        // for garbage collection 
		i1=null;
		
		//now two objects are eligible for
        // garbage collection 
		i2=null;
		
		 // calling garbage collector
        System.gc();

	}
	
	@Override
    protected void finalize() throws Throwable 
    { 
        System.out.println("Finalize method called"); 
    }

}
