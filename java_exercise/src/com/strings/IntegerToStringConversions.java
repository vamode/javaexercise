package com.strings;

import java.text.DecimalFormat;

public class IntegerToStringConversions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
			// toString() method is using new operator to create string object.
		 	int a = 1234;
		    int b = -1234;
		    
		    String str1 = Integer.toString(a);
		    String str2 = Integer.toString(b);
		    
		    System.out.println("String str1 = " + str1); 
		    System.out.println("String str2 = " + str2);
		    
		    /*-----------------------------------------*/
		    
		    // valueOf() method is using toString() method to create string object.
		    int c = 1234;
		    String str3 = String.valueOf(c);
		    System.out.println("String str3 = " + str3);
		    
		    String str4 = String.valueOf(1234);
		    System.out.println("String str4 = " + str4);
		    
		    	    
		    int d = 1234;
		    Integer obj = new Integer(d);
		    String str5 = obj.toString();
		    System.out.println("String str5 = " + str5);
		    
		    String str6 = new Integer(d).toString();
		    System.out.println("String str6 = " + str6);
		    
		    String str7 = new Integer(1234).toString();
		    System.out.println("String str7 = " + str7);
		
		    /*-----------------------------------------*/
		    
		    int e = 12345;
		    DecimalFormat df = new DecimalFormat("#");
		    String str8 = df.format(e);
		    System.out.println("String str8 = "+str8);
		    
		    DecimalFormat df1 = new DecimalFormat("#,###");
		    String str9 = df1.format(e);
		    System.out.println("String str9 = "+str9);
		    
		    /*-----------------------------------------*/
		    
		    int f = 1234;
		    StringBuffer sb = new StringBuffer();
		    sb.append(f);
		    String str10 = sb.toString();
		    System.out.println("String str10 = " + str10);
		    
		    String str11 = new StringBuffer().append(1234).toString();
		    System.out.println("String str11 = " + str11);
		    
		    int g = 1234;
		    sb.append(g);
		    String str12 = sb.toString();
		    System.out.println("String str12 = " + str12);
		    
		    String str13 = new StringBuilder().append(1234).toString();
		    System.out.println("String str13 = " + str13);
		
		    /*-----------------------------------------*/
		    
		    int h = 255;
		    String binaryString = Integer.toBinaryString(h);
		    System.out.println(binaryString);
		    
		    int i = 255;
		    String octalString = Integer.toOctalString(i);
		    System.out.println(octalString);
		    
		    int j = 255;
		    String hexString = Integer.toHexString(j);
		    System.out.println(hexString);
		    
		    int k = 255;
		    String customString = Integer.toString(k, 7);
		    System.out.println(customString);

	}

}
