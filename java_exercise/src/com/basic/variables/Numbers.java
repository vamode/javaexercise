package com.basic.variables;

public class Numbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*
		 * when a 0 is prefixed the value is considered octal, since 12 in octal is 10 in decimal, 
		 * the result is 10. Similarly, if i = 0112, result will be 74 (in decimal).
		 * 
		 */ 
		
		int x = 012;
        System.out.print(x);
        
        System.out.println("\n");
        
        /*
         * Java takes the numbers before the strings are introduced as int and once the string literals are introduced,
         *  all the following numbers are considered as strings.         *  
         */
        
        String s = 3 + 2 + "hello" + 6 + 4;
        System.out.print(s);
        
        System.out.println("\n");
        
        String str = 7*8+"XYZ";
        System.out.print(str);
        
        System.out.println("\n");
        
        String str1 = 10/2+"LMN";
        System.out.print(str1);
        
       

	}

}
