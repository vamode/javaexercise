package com.interfaces_and_abstract_classes.interfaces.comparator;

import java.util.ArrayList;
import java.util.Collections;

public class MainClass {
	
	/*
	
	Comparator interface is used to order the objects of user-defined classes. A comparator object is capable of comparing 
	two objects of two different classes. Following function compare obj1 with obj2
	
	By changing the return value in inside compare method you can sort in any order you want. 
	eg.for descending order just change the positions of a and b in above compare method.

	*/
	
	public static void main (String[] args)
    {
        ArrayList<Student> ar = new ArrayList<Student>();
        ar.add(new Student(111, "bbbb", "london"));
        ar.add(new Student(131, "aaaa", "nyc"));
        ar.add(new Student(121, "cccc", "jaipur"));
 
        System.out.println("Unsorted");
        for (int i=0; i<ar.size(); i++)
            System.out.println(ar.get(i));
 
        Collections.sort(ar, new Sortbyrollno());
 
        System.out.println("\nSorted by rollno");
        for (int i=0; i<ar.size(); i++)
            System.out.println(ar.get(i));
 
        Collections.sort(ar, new Sortbyname());
 
        System.out.println("\nSorted by name");
        for (int i=0; i<ar.size(); i++)
            System.out.println(ar.get(i));
    }

}
