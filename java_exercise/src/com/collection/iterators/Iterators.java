package com.collection.iterators;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;

public class Iterators {
	
	/*
		Iterators are used in Collection framework in Java to retrieve elements one by one. There are three iterators.
		
		1. Enumeration
		2. Iterator
		3. ListIterator
		
		Important Common Points

		1 : Please note that initially any iterator reference will point to the index just before the index of 
		    first element in a collection.
		
		2 : We don�t create objects of Enumeration, Iterator, ListIterator because they are interfaces. 
		    We use methods like elements(), iterator(), listIterator() to create objects. 
		    These methods have anonymous Inner classes that extends respective interfaces and return this class object. 
		    This can be verified by below code. For more on inner class refer
		
	*/
	
	
	public static void main(String[] args) {
		
		Vector v = new Vector();
		 
        // Create three iterators
        Enumeration e = v.elements();
        Iterator  itr = v.iterator();
        ListIterator ltr = v.listIterator();
 
        // Print class names of iterators
        System.out.println("Enumeration : "+e.getClass().getName());
        System.out.println("Iterator : "+itr.getClass().getName());
        System.out.println("ListIterator : "+ltr.getClass().getName());
		
	}
	
	/*
	 
	 	OUTPUT
	 	
	 	Enumeration : java.util.Vector$1
		Iterator : java.util.Vector$Itr
		ListIterator : java.util.Vector$ListItr
		
		
		Note : The $ symbol in reference class name is a proof that concept of inner classes is used and these class objects are created.
		
	
	
	*/
	
}
