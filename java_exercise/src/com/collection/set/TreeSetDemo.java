package com.collection.set;

import java.util.Collections;
import java.util.TreeSet;

public class TreeSetDemo {

	/*
	
	java.util.TreeSet is implementation class of SortedSet Interface. TreeSet has following important properties.

	- TreeSet implements the SortedSet interface so duplicate values are not allowed.
	
	- TreeSet does not preserve the insertion order of elements but elements are sorted by keys.
	
	- TreeSet does not allow to insert Heterogeneous objects. It will throw classCastException at 
	  Runtime if trying to add hetrogeneous objects.
	
	- TreeSet allows to insert null but just once.
	
	- TreeSet is basically implementation of a self-balancing binary search tree like Red-Black Tree. 
	  Therefore operations like add, remove and search take O(Log n) time. And operations like printing n 
	  elements in sorted order takes O(n) time.
		
	
	Synchronized TreeSet:
		Implementation of TreeSet class is not synchronized. If there is need of synchronized version of TreeSet, 
		it can be done externally using Collections.synchronizedSet() method.

		TreeSet ts = new TreeSet();
		Set syncSet = Collections.synchronziedSet(ts);

	
	*/
	public static void main(String[] args) {
		
		sampleProgram();
		nullInsertion();

	}

	private static void sampleProgram() {
		
		TreeSet ts1= new TreeSet();
        ts1.add("A");
        ts1.add("B");
        ts1.add("C");
 
        // Duplicates will not get insert
        ts1.add("C");
 
        // Elements get stored in default natural
        // Sorting Order(Ascending)
        System.out.println(ts1);  // [A,B,C]
 
        // ts1.add(2) ; will throw ClassCastException
        //             at run time
		
	}

	private static void nullInsertion() {
		
		/*
		
		Null Insertion:
			If we insert null in non empty TreeSet, it throws NullPointerException because while inserting null 
			it will get compared to existing elements and null can not be compared to any value
		
		*/
		
		TreeSet ts2= new TreeSet();
        ts2.add("A");
        ts2.add("B");
        ts2.add("C");
        ts2.add(null); // Throws NullPointerException
        
        /*
        
        We can insert first value as null, but if we insert any more value in TreeSet, it will also throw NullPointerException.
        
        */
        TreeSet ts3 = new TreeSet();
        ts3.add(null);
        ts3.add("A");  // Throws NullPointerException
	}

}
