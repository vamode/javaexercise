package com.interfaces_and_abstract_classes.interfaces;

public class B implements A{
	
	/*
	
	If we change fun() to anything other than public in class B, 
	we get compiler error �attempting to assign weaker access privileges; was public�
	
	*/
	
	
	// If we change public to anything else,
    // we get compiler error

	@Override
	public void fun() {
		
	}

}
