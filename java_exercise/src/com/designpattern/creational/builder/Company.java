package com.designpattern.creational.builder;

public abstract class Company implements Packing{
	public abstract int price();
}
