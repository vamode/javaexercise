package com.basic;

public class Interesting_facts_about_null {
	
	private static Object obj; 

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		/*-------------------- 1. null is Case sensitive --------------------*/
		
		// compile-time error : can't find symbol 'NULL'
		//Object obj1 = NULL;
		
		//runs successfully
		Object obj2 = null;
		
		/*-------------------- 2. Reference Variable value --------------------*/
		
		// it will print null;
        System.out.println("Value of object obj is : " + obj);
        System.out.println("---------------------------------------------------------");
        
        /*-------------------- 3. Type of null --------------------*/
        
        // null can be assigned to String
        String str = null; 
        System.out.println("str : "+str);
        
        // you can assign null to Integer also
        Integer itr = null; 
        System.out.println("itr : "+itr);
    	
        // null can also be assigned to Double
        Double dbl = null;
        System.out.println("dbl : "+dbl);
            
        // null can be type cast to String
        String myStr = (String) null; 
        System.out.println("myStr : "+myStr);
    	
        // it can also be type casted to Integer
        Integer myItr = (Integer) null; 
        System.out.println("myItr : "+myItr);
    	
        // yes it's possible, no error
        Double myDbl = (Double) null; 
        System.out.println("myDbl : "+myDbl);
        
        System.out.println("---------------------------------------------------------");
        
        /*-------------------- 4. Autoboxing and unboxing --------------------*/
        
        //An integer can be null, so this is fine
        Integer i = null;
         
        //Unboxing null to integer throws NullpointerException
        //int a = i;
        //System.out.println("---------------------------------------------------------");
        
        /*-------------------- 5. instanceof operator --------------------*/
        
        Integer i1 = null;
        Integer j1 = 10;
             
        //prints false
        System.out.println(i1 instanceof Integer);
         
        //Compiles successfully
        System.out.println(j1 instanceof Integer);
        System.out.println("---------------------------------------------------------");
        
        /*-------------------- 6. Static vs Non static Methods --------------------*/
        
        Interesting_facts_about_null obj= null;
        obj.staticMethod();
        //obj.nonStaticMethod();
        System.out.println("---------------------------------------------------------");
		
        /*-------------------- 7. == and != --------------------*/
        
      //return true;
        System.out.println(null==null);
         
        //return false;
        System.out.println(null!=null);
        
        
        
	}
	
	private static void staticMethod()
    {
        //Can be called by null reference
        System.out.println("static method, can be called by null reference");
         
    }
         
    private void nonStaticMethod()
    {
        //Can not be called by null reference
        System.out.print(" Non-static method- ");
        System.out.println("cannot be called by null reference");
         
    }

}
