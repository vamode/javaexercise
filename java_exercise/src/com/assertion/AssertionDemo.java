package com.assertion;

public class AssertionDemo {
	
	/*
	
	An assertion allows testing the correctness of any assumptions that have been made in the program.

	Assertion is achieved using the assert statement in Java. While executing assertion, 
	it is believed to be true. If it fails, JVM throws an error named AssertionError. 
	It is mainly used for testing purposes during development.

	The assert statement is used with a Boolean expression and can be written in two different ways.
	
	First way :
		assert expression;
	
	Second way :
		assert expression1 : expression2;
		
	Enabling Assertions :

	By default, assertions are disabled. We need to run the code as given. The syntax for enabling 
	assertion statement in Java source code is:
		java �ea Test
		
		eclipse : write �ea in Run Configuration ---> Arguments ---> VM Arguments
		
	Disabling Assertions :
		java �da Test
		
		eclipse : write �da OR remove -ea in Run Configuration ---> Arguments ---> VM Arguments 
	
	*/
	
	public static void main(String[] args) {
		
		int value = 15;
        assert value >= 20 : " Underweight";
        System.out.println("value is "+value);
		
	}

}
