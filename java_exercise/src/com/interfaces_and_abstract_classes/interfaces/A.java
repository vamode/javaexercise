package com.interfaces_and_abstract_classes.interfaces;

public interface A {
	
	/*
	
	There is a rule that every member of interface is only and only public whether you define or not. 
	So when we define the method of the interface in a class implementing the interface, 
	we have to give it public access as child class can�t assign the weaker access to the methods.
	
	
	*/
	
	void fun();
}
