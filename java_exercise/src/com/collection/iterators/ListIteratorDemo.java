package com.collection.iterators;

import java.util.ArrayList;
import java.util.ListIterator;

public class ListIteratorDemo {
	
	/*
	
	It is only applicable for List collection implemented classes like arraylist, linkedlist etc. 
	It provides bi-directional iteration.
	
	ListIterator must be used when we want to enumerate elements of List. 
	This cursor has more functionality(methods) than iterator.

	ListIterator object can be created by calling listIterator() method present in List interface.
	
	ListIterator interface extends Iterator interface. 
	So all three methods of Iterator interface are available for ListIterator. 
	In addition there are six more methods.
	
	public boolean hasNext();
	public Object next();
	public int nextIndex();
	public boolean hasPrevious();
	public Object previous();
	public int previousIndex();
	public void remove();
	public void set(Object obj);
	public void add(Object obj);
	
	
	set() method can throw four exceptions :

	- UnsupportedOperationException � if the set operation is not supported by this list iterator
	- ClassCastException : If the class of the specified element prevents it from being added to this list
	- IllegalArgumentException : If some aspect of the specified element prevents it from being added to this list
	- IllegalStateException : If neither next nor previous have been called, or remove or add have been called 
							  after the last call to next or previous
	
	
	add() method can throw three exceptions :
	
	- UnsupportedOperationException : If the add method is not supported by this list iterator
	- ClassCastException : If the class of the specified element prevents it from being added to this list
	- IllegalArgumentException : If some aspect of this element prevents it from being added to this list
	
	
	Limitations of ListIterator : 
	It is the most powerful iterator but it is only applicable for List implemented classes, so it is not a universal iterator.
		
	
	*/

	public static void main(String[] args) {
		
		ArrayList al = new ArrayList();
        for (int i = 0; i < 10; i++)
            al.add(i);
 
        System.out.println("Before : "+al);
 
        // at beginning ltr(cursor) will point to
        // index just before the first element in al
        ListIterator ltr = al.listIterator();
 
        // checking the next element availabilty
        while (ltr.hasNext())
        {
            //  moving cursor to next element
            int i = (Integer)ltr.next();
 
            // getting even elements one by one
            System.out.print(i + " ");
 
            // Changing even numbers to odd and
            // adding modified number again in 
            // iterator
            if (i%2==0)
            {
                i++;  // Change to odd
                ltr.set(i);  // set method to change value
                ltr.add(i);  // to add
            }
        }
        System.out.println();
        System.out.println("After : "+al);
        System.out.println("Using previous method : ");
        
        while(ltr.hasPrevious())
            System.out.print(ltr.previous()+ " ");

	}

}
