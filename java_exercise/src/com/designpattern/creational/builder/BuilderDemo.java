package com.designpattern.creational.builder;

public class BuilderDemo {

	public static void main(String[] args) {

	   CDBuilder cdBuilder=new CDBuilder();  
	   
	   CDType cdType1=cdBuilder.buildSonyCD();  
	   cdType1.showItem();;  
	  
	   CDType cdType2=cdBuilder.buildSamsungCD();  
	   cdType2.showItem();  
	}

}
