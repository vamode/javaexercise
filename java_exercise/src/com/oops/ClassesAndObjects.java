package com.oops;

public class ClassesAndObjects {

	/*
	 * 
	Class:
	
	A class is a user defined blueprint or prototype from which objects are created.  
	It represents the set of properties or methods that are common to all objects of one type. 
	In general, class declarations can include these components
	
	1. Modifiers 2. Class name 3. Superclass 4. Interfaces 5. Body
	
	Object: 
	
	Objects correspond to things found in the real world. For example, a graphics program may have 
	objects such as �circle�, �square�, �menu�. An online shopping system might have objects such 
	as �shopping cart�, �customer�, and �product�.
	
	*/

}
