package com.interfaces_and_abstract_classes.abstract_classes;

public class Derived extends Base{
	
	Derived() {
		System.out.println("Derived Constructor Called");
	}

	@Override
	void fun() {
		
		System.out.println("Derived fun() called");
		
	}

}
