package com.basic.variables;

public class StaticVariables {

	static int i = 10;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		StaticVariables sv = new StaticVariables();
		
		// static int a = 10; Error : Illegal modifier for parameter a; only final is permitted
		
		System.out.println("i : "+i);
		
		//fun(); Cannot make a static reference to the non-static method fun() from the type StaticVariables
		sv.fun();
		
		Test2 t = new Test2();
		
		Test2 t1 = new Test2();
		System.out.println("t1.name : "+t1.name);
		System.out.println("t.name : "+t.name);
		
		Test2 t2 = new Test2();
		t2.name = "Siddhant";
		System.out.println("t2.name : "+t2.name);
		System.out.println("t.name : "+t.name);
		
		Test2 t3 = new Test2();
		System.out.println("t3.name : "+t3.name);
		System.out.println("t.name : "+t.name);
		
		
		
		
	}
	
	void fun(){
		System.out.println("static i in non static function : "+i);
	}

}

class Test2{
	static String name = "Vishal";
}
