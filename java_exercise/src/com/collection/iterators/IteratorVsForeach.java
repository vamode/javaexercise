package com.collection.iterators;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class IteratorVsForeach {
	
	/*
	
	Iterator: 
		Iterator is an interface provided by collection framework to traverse a collection and 
		for a sequential access of items in the collection.
	
	Foreach: 
		Foreach loop is meant for traversing items in a collection.
		
	Note : In Java 8 using lambda expressions we can simply replace for-each loop with
	
		   elements.forEach (e -> System.out.println(e) );
		   
		   
    Difference between the two traversals :

	- In for-each loop, we can�t modify collection, it will throw a ConcurrentModificationException on the other 
	  hand with iterator we can modify collection.
	
	- Modifying a collection simply means removing an element or changing content of an item stored in the collection. 
	  This occurs because for-each loop implicitly creates an iterator but it is not exposed to the user thus we can�t 
	  modify the items in the collections.
	
	When to use which traversal?
	
	- If we have to modify collection, we must use an Iterator.
	- While using nested for loops it is better to use for-each loop, consider the below code for better understanding.		   
	
	*/

	public static void main(String[] args) {
		
		/*
			The methodForIterator() throws java.util.NoSuchElementException.
			
			In the above code we are calling the next() method again and again for itr1 (i.e., for List l). 
			Now we are advancing the iterator without even checking if it has any more elements left in the 
			collection(in the inner loop), thus we are advancing the iterator more than the number of elements 
			in the collection which leads to NoSuchElementException.
			
			for-each loops are tailor made for nested loops. Replace the iterator code with the below code.
			
			Iterator and for-each loop are faster than simple for loop for collections with no random access, 
			while in collections which allows random access there is no performance change with for-each loop/for loop/iterator.
			
		*/
		
		//methodForIterator();
		
		methodForEach();
	}
	
	public static void methodForIterator(){
		
		// Create a link list which stores integer elements
        List<Integer> l = new LinkedList<Integer>();
 
        // Now add elements to the Link List
        l.add(2);
        l.add(3);
        l.add(4);
 
        // Make another Link List which stores integer elements
        List<Integer> s=new LinkedList<Integer>();
        s.add(7);
        s.add(8);
        s.add(9);
 
        // Iterator to iterate over a Link List
        for (Iterator<Integer> itr1=l.iterator(); itr1.hasNext(); )
        {
            for (Iterator<Integer> itr2=s.iterator(); itr2.hasNext(); )
            {
                if (itr1.next() < itr2.next())
                {
                    System.out.println(itr1.next());
                }
            }
        }
		
	}
	
	public static void methodForEach(){
		
		// Create a link list which stores integer elements
        List<Integer> l=new LinkedList<Integer>();
 
        // Now add elements to the Link List
        l.add(2);
        l.add(3);
        l.add(4);
 
        // Make another Link List which stores integer elements
        List<Integer> s=new LinkedList<Integer>();
        s.add(2);
        s.add(4);
        s.add(5);
        s.add(6);
 
        // Iterator to iterate over a Link List
        for (int a:l)
        {
            for (int b:s)
            {
                if (a<b)
                    System.out.print(a + " ");
            }
        }
		
	}

}
