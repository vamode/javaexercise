package com.strings;

public class SplitMethod {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String str = "geekss@for@geekss";
        String [] arrOfStr = str.split("@", 2);
 
        for (String a : arrOfStr)
            System.out.println("1 = "+a);
        
        String [] arrOfStr1 = str.split("@", 5);
 
        for (String a : arrOfStr1)
            System.out.println("2 = "+a);
        
        String [] arrOfStr2 = str.split("@", -2);
 
        for (String a : arrOfStr2)
            System.out.println("3 = "+a);
        
        
        String [] arrOfStr3 = str.split("s", 5);
 
        for (String a : arrOfStr3)
            System.out.println("4 = "+a);
        
        
        String [] arrOfStr4 = str.split("s",-2);
 
        for (String a : arrOfStr4)
            System.out.println("5 = "+a);
        
        String [] arrOfStr5 = str.split("s",0);
        
        for (String a : arrOfStr5)
            System.out.println("6 = "+a);

        /*-------------------------------------------------------------------*/
        
        String str1 = "GeeksforGeeks:A Computer Science Portal";
        String [] arrOfStr6 = str1.split(":");
 
        for (String a : arrOfStr6)
            System.out.println("7 = "+a);
        
        String [] arrOfStr7 = str1.split("for");
        
        for (String a : arrOfStr7)
            System.out.println("8 = "+a);
        
        String [] arrOfStr8 = str1.split(" ");
        
        for (String a : arrOfStr8)
            System.out.println("9 = "+a);
        
        String [] arrOfStr9 = str1.split("s");
        
        for (String a : arrOfStr9)
            System.out.println("10 = "+a);
        
        String [] arrOfStr10 = str1.split("for");
        
        for (String a : arrOfStr10)
            System.out.println("11 = "+a);
        
	}

}
