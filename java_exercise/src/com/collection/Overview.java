package com.collection;

public class Overview {
	
	/*
	
	A Collection is a group of individual objects represented as a single unit. Java provides Collection Framework 
	which defines several classes and interfaces to represent a group of objects as a single unit.

	The Collection interface (java.util.Collection) and Map interface (java.util.Map) are two main root interfaces 
	of Java collection classes.
	
									iterator()
					  Iterable<E> ----------------> Iterator<E>
						 |
						 |
			         Collection               Map
		         /     /    \     \            |
		        /     /      \     \           |
		     Set    List    Queue  Dequeue   SortedMap
		     /								   |
		    /								   |
	 SortedSet 								   |
		 /									   |
	NavigableSet<E>                       NavigableMap<E>	 
		 
		            Core Interfaces in Collections
		            
		            
	Implementation classes : 
	
		List  : ArrayList, LinkedList, Stack, Vector(Sync)
		Set   : HashSet, LinkedHashSet, TreeSet(SortedSet)
		Queue : PriorityQueue, ArrayDeque(Deque), LinkedDeque(Deque)
		Map   : HashMap, LinkedHashMap, HashTable(Sync), TreeMap(SortedMap)
		
		
		
		Collection : Root interface with basic methods like add(), remove(), 
             contains(), isEmpty(), addAll(), ... etc.
 
		Set : Doesn't allow duplicates. Example implementations of Set 
		      interface are HashSet (Hashing based) and TreeSet (balanced
		      BST based). Note that TreeSet implements SortedSet.
		
		List : Can contain duplicates and elements are ordered. Example
		       implementations are LinkedList (linked list based) and
		       ArrayList (dynamic array based)
		
		Queue : Typically order elements in FIFO order except exceptions
		        like PriorityQueue.  
		
		Deque : Elements can be inserted and removed at both ends. Allows
		        both LIFO and FIFO. 
		
		Map : Contains Key value pairs. Doesn't allow duplicates.  Example
		      implementation are HashMap and TreeMap. 
		      TreeMap implements SortedMap.        
		
		The difference between Set and Map interface is, in Set we have only
		keys, but in Map, we have key value pairs.
	 

	*/
	
	public static void main(String[] args) {
		
	}
	

}
