package com.collection.queue;

import java.util.Iterator;
import java.util.PriorityQueue;

public class PriorityQueueDemo {
	
	/*
	
	To process the objects in the queue based on the priority, we tend to use Priority Queue.

	Important points about Priority Queue:
	
	- PriorityQueue doesn�t allow null
	
	- We can�t create PriorityQueue of Objects that are non-comparable
	
	- The elements of the priority queue are ordered according to their natural ordering, or by a Comparator provided at 
	  queue construction time, depending on which constructor is used.
	
	- The head of this queue is the least element with respect to the specified ordering. If multiple elements are tied 
	  for least value, the head is one of those elements � ties are broken arbitrarily.
	
	- The queue retrieval operations poll, remove, peek, and element access the element at the head of the queue.
	
	- It inherits methods from AbstractQueue, AbstractCollection, Collection and Object class.
		
	*/

	public static void main(String[] args) {

		// Creating empty priority queue
		PriorityQueue<String> pQueue = new PriorityQueue<String>();

		// Adding items to the pQueue
		pQueue.add("C");
		pQueue.add("C++");
		pQueue.add("Java");
		pQueue.add("Python");

		// Printing the most priority element
		System.out.println("Head value using peek function:" + pQueue.peek());

		// Printing all elements
		System.out.println("The queue elements:");
		Iterator itr = pQueue.iterator();
		while (itr.hasNext())
			System.out.println(itr.next());

		// Removing the top priority element (or head) and
		// printing the modified pQueue
		pQueue.poll();
		System.out.println("After removing an element" + "with poll function:");
		Iterator<String> itr2 = pQueue.iterator();
		while (itr2.hasNext())
			System.out.println(itr2.next());

		// Removing Java
		pQueue.remove("Java");
		System.out.println("after removing Java with" + " remove function:");
		Iterator<String> itr3 = pQueue.iterator();
		while (itr3.hasNext())
			System.out.println(itr3.next());

		// Check if an element is present
		boolean b = pQueue.contains("C");
		System.out.println("Priority queue contains C" + "ot not?: " + b);

		// get objects from the queue in an array and
		// print the array
		Object[] arr = pQueue.toArray();
		System.out.println("Value in array: ");
		for (int i = 0; i < arr.length; i++)
			System.out.println("Value: " + arr[i].toString());

	}

}
