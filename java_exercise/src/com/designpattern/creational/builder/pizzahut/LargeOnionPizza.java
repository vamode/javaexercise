package com.designpattern.creational.builder.pizzahut;

public class LargeOnionPizza extends VegPizza{

	@Override
	public String name() {
		return "Onion Pizza";
	}

	@Override
	public String size() {
		return "Large size";
	}

	@Override
	public float price() {
		return 180.0f;
	}

}
