package com.designpattern.creational.builder;

import java.util.ArrayList;
import java.util.List;

public class CDType {
	private List<Packing> items = new ArrayList<Packing>();
	
	public void addItem(Packing pack){
		items.add(pack);
	}
	
	/*public void getCost(){
		for (Packing packing : items) {
			packing.price();
		}
	}*/
	
	public void showItem(){
		for (Packing packing : items) {
			System.out.print("CD name : "+packing.pack());  
            System.out.println(", Price : "+packing.price()); 
		}
	}
}
