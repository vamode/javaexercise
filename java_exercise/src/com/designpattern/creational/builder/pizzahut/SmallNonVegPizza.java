package com.designpattern.creational.builder.pizzahut;

public class SmallNonVegPizza extends NonVegPizza{

	@Override
	public String name() {
		return "Non-Veg Pizza";
	}

	@Override
	public String size() {
		return "Samll Size";
	}

	@Override
	public float price() {
		return 180.0f;
	}

}
