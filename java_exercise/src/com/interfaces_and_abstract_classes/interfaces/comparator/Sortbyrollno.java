package com.interfaces_and_abstract_classes.interfaces.comparator;

import java.util.Comparator;

public class Sortbyrollno implements Comparator<Student>{
	
	@Override
	public int compare(Student a, Student b) {
		
		// Used for sorting in ascending order of
	    // roll number
		return a.rollno - b.rollno;
	}

}
