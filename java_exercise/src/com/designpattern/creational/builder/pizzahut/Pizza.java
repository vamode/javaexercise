package com.designpattern.creational.builder.pizzahut;

public abstract class Pizza implements Item{
	@Override
	public abstract float price();
}
