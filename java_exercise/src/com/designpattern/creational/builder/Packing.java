package com.designpattern.creational.builder;

public interface Packing {
	public String pack();
	public int price();
}
