package com.collection.map;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapDemo {
	
	/*
	
	The java.util.Map interface represents a mapping between a key and a value. 
	The Map interface is not a subtype of the Collection interface. 
	Therefore it behaves a bit different from the rest of the collection types.
	
	A Map cannot contain duplicate keys and each key can map to at most one value. Some implementations allow null key and null value (HashMap and LinkedHashMap) but some do not (TreeMap).

	The order of a map depends on specific implementations, 
	e.g TreeMap and LinkedHashMap have predictable order, while HashMap does not.
	
	Exampled class that implements this interface is HashMap, TreeMap and LinkedHashMap.
	
	Why and When Use Maps:
		Maps are perfectly for key-value association mapping such as dictionaries. 
		Use Maps when you want to retrieve and update elements by keys, or perform lookups by keys. Some examples:
	
		- A map of error codes and their descriptions.
		- A map of zip codes and cities.
		- A map of managers and employees. Each manager (key) is associated with a list of employees (value) he manages.
		- A map of classes and students. Each class (key) is associated with a list of students (value).
		
		Why and When Use Maps:
		Maps are perfectly for key-value association mapping such as dictionaries. 
		Use Maps when you want to retrieve and update elements by keys, or perform lookups by keys. Some examples:
		
		- A map of error codes and their descriptions.
		- A map of zip codes and cities.
		- A map of managers and employees. Each manager (key) is associated with a list of employees (value) he manages.
		- A map of classes and students. Each class (key) is associated with a list of students (value).
	
	
	*/

	public static void main(String[] args) {

		HashMap<String, Integer> hm = new HashMap<String, Integer>();
		hm.put("a", new Integer(100));
		hm.put("b", new Integer(200));
		hm.put("c", new Integer(300));
		hm.put("d", new Integer(400));

		// Returns Set view
		Set<Map.Entry<String, Integer>> st = hm.entrySet();

		for (Map.Entry<String, Integer> me : st) {
			System.out.print(me.getKey() + ":");
			System.out.println(me.getValue());
		}
		
	}

}
