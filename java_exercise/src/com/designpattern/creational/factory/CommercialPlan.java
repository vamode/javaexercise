package com.designpattern.creational.factory;

public class CommercialPlan extends Plan {

	@Override
	void getRate() {
		rate=7.50;
	}

}
