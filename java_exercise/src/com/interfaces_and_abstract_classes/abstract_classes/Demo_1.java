package com.interfaces_and_abstract_classes.abstract_classes;

public class Demo_1 {

	public static void main(String[] args) {

		// Uncommenting the following line will cause compiler error as the 
        // line tries to create an instance of abstract class.
        // Base b = new Base();
 
        // We can have references of Base type.
        Base b = new Derived();
        b.fun();
        b.funFinal();
        Base.staticMethod();
        
	}

}
