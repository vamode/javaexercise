package com.designpattern.creational.builder.pizzahut;

public class LargeCoke extends Coke{

	@Override
	public String name() {
		// TODO Auto-generated method stub
		return "750 ml Coke";
	}

	@Override
	public String size() {
		// TODO Auto-generated method stub
		return "Large Size";
	}

	@Override
	public float price() {
		// TODO Auto-generated method stub
		return 50.0f;
	}

}
