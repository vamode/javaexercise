package com.oops;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Constructor;

public class JavaObjectStorageAndCreation implements Cloneable,Serializable{
	
	/*
	
	In Java, all objects are dynamically allocated on Heap.
	
	when we only declare a variable of a class type, only a reference is created (memory is not allocated for the object). 
	To allocate memory to an object, we must use new(). So the object is always allocated memory on heap
	
	
	*/
	
	private String name = "Vishal Amode";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/* Different ways to create objects in Java */
		
		usingNewKeyword();
		usingNewInstance();
		usingCloneMethod();
		usingDeserialization();
		usingNewInstanceMethodOfConstructor();

	}

	private static void usingNewKeyword() {
		

        // Here we are creating Object of 
        // NewKeywordExample using new keyword
		JavaObjectStorageAndCreation obj = new JavaObjectStorageAndCreation();
        System.out.println("usingNewKeyword : "+obj.name);
		
	}

	private static void usingNewInstance() {
		
		/*
		
		If we know the name of the class & if it has a public default constructor we can create an 
		object �Class.forName. We can use it to create the Object of a Class. Class.forName actually 
		loads the Class in Java but doesn�t create any Object. To Create an Object of the Class you 
		have to use the new Instance Method of the Class.
		
		*/
		
		Class cls;
		try {
			cls = Class.forName("JavaObjectStorageAndCreation");
			JavaObjectStorageAndCreation obj =
					(JavaObjectStorageAndCreation) cls.newInstance();
			System.out.println("usingNewInstance : "+obj.name);
		} catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (InstantiationException e)
        {
            e.printStackTrace();
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
		
	}
	
	@Override
    protected Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }

	private static void usingCloneMethod() {
		
		/*
		
		Whenever clone() is called on any object, the JVM actually creates a new object and copies all content 
		of the previous object into it. Creating an object using the clone method does not invoke any constructor.
		
		To use clone() method on an object we need to implement Cloneable and define the clone() method in it.
		
		Note :

			Here we are creating the clone of an existing Object and not any new Object.
			Class need to implement Cloneable Interface otherwise it will throw CloneNotSupportedException.
		
		*/
		
		JavaObjectStorageAndCreation obj1 = new JavaObjectStorageAndCreation();
        try
        {
        	JavaObjectStorageAndCreation obj2 = (JavaObjectStorageAndCreation) obj1.clone();
            System.out.println("usingCloneMethod : "+obj2.name);
        }
        catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
        }
        
	}

	private static void usingDeserialization() {
		/*
		
		Whenever we serialize and then deserialize an object, JVM creates a separate object. 
		In deserialization, JVM doesn�t use any constructor to create the object.
		
		To deserialize an object we need to implement the Serializable interface in the class.
		
		*/
		
		try {
			
			/* Serialization */
			
			JavaObjectStorageAndCreation d =
			        new JavaObjectStorageAndCreation();
			FileOutputStream f = new FileOutputStream("file.txt");
			ObjectOutputStream oos = new ObjectOutputStream(f);
			oos.writeObject(d);
			oos.close();
			f.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/* De-Serialization */
		
		try {
			JavaObjectStorageAndCreation d;
            FileInputStream f = new FileInputStream("file.txt");
            ObjectInputStream oos = new ObjectInputStream(f);
            d = (JavaObjectStorageAndCreation)oos.readObject();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		
	}

	private static void usingNewInstanceMethodOfConstructor() {
		
		/*
		
		This is similar to the newInstance() method of a class. There is one newInstance() method 
		in the java.lang.reflect.Constructor class which we can use to create objects. 
		It can also call parameterized constructor, and private constructor by using this newInstance() method.

		Both newInstance() methods are known as reflective ways to create objects. In fact newInstance() method of 
		Class internally uses newInstance() method of Constructor class.
		
		*/
		
		try
        {
            Constructor<JavaObjectStorageAndCreation> constructor
                = JavaObjectStorageAndCreation.class.getDeclaredConstructor();
            JavaObjectStorageAndCreation r = constructor.newInstance();
            System.out.println("usingNewInstanceMethodOfConstructor : "+r.name);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
		
	}

}
