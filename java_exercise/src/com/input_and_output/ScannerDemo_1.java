package com.input_and_output;

import java.util.Scanner;

public class ScannerDemo_1 {
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		String name = scanner.nextLine();
		
		char gender = scanner.next().charAt(0);
		
		int age = scanner.nextInt();
		
		long mobileNo = scanner.nextLong();
		
		double cgpa = scanner.nextDouble();
		
		System.out.println("Name : "+name);
		System.out.println("Gender : "+gender);
		System.out.println("Age : "+age);
		System.out.println("Mobile Number : "+mobileNo);
		System.out.println("CGPA : "+cgpa);
		
	}

}
